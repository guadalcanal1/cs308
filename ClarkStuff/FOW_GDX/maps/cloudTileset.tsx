<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Cloud_Full" tilewidth="256" tileheight="256" tilecount="20" columns="4">
 <image source="icons/Cloud_Full.png" width="1024" height="1280"/>
 <tile id="0">
  <properties>
   <property name="animated" value="true"/>
   <property name="name" value="cloud"/>
  </properties>
 </tile>
</tileset>
