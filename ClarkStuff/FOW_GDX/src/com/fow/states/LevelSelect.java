package com.fow.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.fow.handlers.GameButton;
import com.fow.handlers.GameStateManager;
import com.fow.main.Game;

public class LevelSelect extends GameState {
	
	
	private GameButton[][] buttons;
	
    private Skin skin;
    private Stage stage;
	
	public LevelSelect(GameStateManager gsm) {
		
		super(gsm);
		
		skin = new Skin(Gdx.files.internal("data/uiskin.json"));
        stage = new Stage();

        final TextButton button = new TextButton("Click me", skin, "default");
        
        button.setWidth(200f * Game.SCALE);
        button.setHeight(20f * Game.SCALE);
        button.setPosition(Gdx.graphics.getWidth() /2 - 100f * Game.SCALE, Gdx.graphics.getHeight()/2 - 10f * Game.SCALE);
        
        button.addListener(new ClickListener(){
            @Override 
            public void clicked(InputEvent event, float x, float y){
            	gsm.setState(GameStateManager.PLAY);
                button.setText("You clicked the button");
                Game.res.getSound("menu_select").play();
            }
        });
        
        stage.addActor(button);
        
        Gdx.input.setInputProcessor(stage);

		cam.setToOrtho(false, Game.V_WIDTH, Game.V_HEIGHT);
	}
	
	public void handleInput() {
	}
	
	public void update(float dt) {
		handleInput();
	}
	
	public void render() {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
		sb.setProjectionMatrix(cam.combined);
		
		sb.begin();
		stage.draw();
		sb.end();
		
	}
	
	public void dispose() {
		// everything is in the resource manager com.neet.blockbunny.handlers.Content
	}
	
}
