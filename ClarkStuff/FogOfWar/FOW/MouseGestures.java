import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.PickResult;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

    public class MouseGestures {

    	boolean showHoverCursor = true;
    	
        public void makePaintable( Node node) {


            // that's all there is needed for hovering, the other code is just for painting
            if( showHoverCursor) {
                node.hoverProperty().addListener(new ChangeListener<Boolean>(){

                    @Override
                    public void changed(ObservableValue<? extends Boolean> observable,
                    		Boolean oldValue, Boolean newValue) {

                        if( newValue) {
                            ((Tile) node).hoverHighlight();
                        } else {
                            ((Tile) node).hoverUnhighlight();
                        }

                        //for( String s: node.getStyleClass())
                    }

                });
            }

            node.setOnMousePressed( onMousePressedEventHandler);
            node.setOnDragDetected( onDragDetectedEventHandler);
            node.setOnMouseDragEntered(onMouseDragEnteredEventHandler);

        }

        EventHandler<MouseEvent> onMousePressedEventHandler = event -> {

            Tile cell = (Tile) event.getSource();

            if( event.isPrimaryButtonDown()) {
                cell.select();
            } else if( event.isSecondaryButtonDown()) {
                cell.deselect();
            }
        };

        EventHandler<MouseEvent> onMouseDraggedEventHandler = event -> {

            PickResult pickResult = event.getPickResult();
            Node node = pickResult.getIntersectedNode();

            if( node instanceof Tile) {

                Tile cell = (Tile) node;

                if( event.isPrimaryButtonDown()) {
                    cell.select();
                } else if( event.isSecondaryButtonDown()) {
                    cell.deselect();
                }

            }

        };

        EventHandler<MouseEvent> onMouseReleasedEventHandler = event -> {
        };

        EventHandler<MouseEvent> onDragDetectedEventHandler = event -> {

            Tile cell = (Tile) event.getSource();
            cell.startFullDrag();

        };

        EventHandler<MouseEvent> onMouseDragEnteredEventHandler = event -> {

            Tile cell = (Tile) event.getSource();

            if( event.isPrimaryButtonDown()) {
                cell.select();
            } else if( event.isSecondaryButtonDown()) {
                cell.deselect();
            }

        };

    }