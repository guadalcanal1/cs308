
import java.io.*;
import java.net.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Date;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.concurrent.CountDownLatch;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

public class FOWServer extends Application
    implements TicTacToeConstants {
      public static final CountDownLatch latch = new CountDownLatch(1);
      public static FOWServer FOWserver = null;

       public static FOWServer waitForServer() {
           try {
               latch.await();
           } catch (InterruptedException e) {
               e.printStackTrace();
           }
           return FOWserver;
       }

       public static void setFOWServer(FOWServer FOWServer0) {
         FOWserver = FOWServer0;
         latch.countDown();
       }

       public FOWServer() {
         setFOWServer(this);
       }

  private int sessionNo = 1; // Number a session
  LinkedList<userSocket> activePlayers = new LinkedList<>();


  @Override // Override the start method in the Application class
  public void start(Stage primaryStage) {
	TextArea taLog = new TextArea();

    // Create a scene and place it in the stage
    Scene scene = new Scene(new ScrollPane(taLog), 450, 200);
    primaryStage.setTitle("TicTacToeServer"); // Set the stage title
    primaryStage.setScene(scene); // Place the scene in the stage
    primaryStage.show(); // Display the stage

    new Thread( () -> {
      try {
        // Create a server socket
    	InetAddress addr = InetAddress.getByName("127.0.0.1");
        ServerSocket serverSocket = new ServerSocket(8000,50,addr);
        Platform.runLater(() -> taLog.appendText(new Date() +
          ": Server started at socket 8000\n"));
        /*ServerSocket gameBrowserServerSocket = new ServerSocket(8081);
		Platform.runLater(() -> taLog.appendText(new Date()+
			": Server started at socket 8081\n"));
		*/
        // Ready to create a session for every two players
        while (true) {
          Platform.runLater(() -> taLog.appendText(new Date() +
            ": Wait for players to join session " + sessionNo + '\n'));

          Socket holdSocket = serverSocket.accept();
          try {
				addUser(holdSocket);
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}

          Platform.runLater(() -> {
        	  String tempName = activePlayers.getLast().getUser().getName();
            taLog.appendText(new Date() + ": Player " + tempName + " joined session "
              + sessionNo + '\n');
            taLog.appendText(tempName+"'s IP address" +
            		activePlayers.getLast().getSocket().getInetAddress().getHostAddress() + '\n');
          });

          new Thread(new gameBrowserSessionHandler(activePlayers));

        }
      }
      catch(IOException ex) {
        ex.printStackTrace();
      }
    }).start();
  }

  private boolean addUser(Socket holdSocket) throws IOException, ClassNotFoundException {
	  new DataOutputStream(holdSocket.getOutputStream()).writeInt(1);
      Object holdObject = new ObjectInputStream(holdSocket.getInputStream()).readObject();

      User holdUser = null;
      if(holdObject instanceof User) {
      	holdUser = (User) holdObject;
      }
      System.out.println("User: "+holdUser.getName());
      updateUserData("state","In-Menus",holdUser.getID());
      activePlayers.offer(new userSocket(holdSocket,holdUser));
      return true;
	}
  public void updateUserData(String col, String newData, int userid) {
	  try {Class.forName("com.mysql.jdbc.Driver");
      Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/FogOFWar", "root", "");
      Statement smt = con.createStatement(); //Create Statement to interact

      smt.executeUpdate("UPDATE `users` SET `" + col + "` = '" + newData + "' WHERE `UserId` = " + userid);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  //finds and returns a user by its name
  public userSocket findUser(String name) {
	  ListIterator<userSocket> itr = activePlayers.listIterator(0);
	  userSocket current = activePlayers.getFirst();
	  while(itr.hasNext()) {
		  if(current.getUser().getName() == name) {
			  return current;
		  } else {
			  current = itr.next();
		  }
	  }
	  return null;
  }

  class gameBrowserSessionHandler implements Runnable{

	LinkedList<userSocket> activePlayers;

	public gameBrowserSessionHandler(LinkedList<userSocket> ap) {
		activePlayers = ap;

	}

	@Override
	public void run() {
		try {
		ListIterator<userSocket> ltr = activePlayers.listIterator(0);
		userSocket curr = activePlayers.getFirst();
		while(true) {
			if(curr.getUser().getStatus() == "In-Menus") {
				Object obj = new ObjectInputStream(
						curr.getSocket().getInputStream()).readObject();
			      if(obj instanceof requestUser) {
			    	  requestUser ru = (requestUser) obj;
			    	  if(ru.getRequestId() == 100) {
			    		  new GameSessionHandler(curr,findUser(ru.getUser().getName()));
			    	  }
			      }
			}
			curr = ltr.next();
		}

		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

  }

  class GameSessionHandler implements Runnable, TicTacToeConstants {

	private userSocket p1;
	private userSocket p2;

    // Create and initialize cells
    private char[][] cell =  new char[20][20];

    //Data Streams
    private DataInputStream DfromP1;
    private DataOutputStream DtoP1;
    private DataInputStream DfromP2;
    private DataOutputStream DtoP2;
    //Object Streams
    private ObjectInputStream OfromP1;
    private ObjectOutputStream OtoP1;
    private ObjectInputStream OfromP2;
    private ObjectOutputStream OtoP2;

    // Continue to play
    private boolean continueToPlay = true;

	public GameSessionHandler(userSocket player1, userSocket player2) {
		p1 = player1;
		p2 = player2;
	}

	@Override
	public void run() {
		try {
		// Create data input and output streams
        DfromP1 = new DataInputStream(
          p1.getInputStream());
        DtoP1 = new DataOutputStream(
          p1.getOutputStream());
        DfromP2 = new DataInputStream(
          p2.getInputStream());
		DtoP2 = new DataOutputStream(
		  p2.getOutputStream());

		// Create Object input and output streams
        OfromP1 = new ObjectInputStream(
          p1.getInputStream());
        OtoP1 = new ObjectOutputStream(
          p1.getOutputStream());
        OfromP2 = new ObjectInputStream(
          p2.getInputStream());
		OtoP2 = new ObjectOutputStream(
		  p2.getOutputStream());

		DtoP1.writeInt(1);
		DtoP2.writeInt(2);

          } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

  }


// Define the thread class for handling a new session for two players
  class HandleASession implements Runnable, TicTacToeConstants {
    private Socket player1;
    private Socket player2;

    // Create and initialize cells
    private char[][] cell =  new char[20][20];

    private DataInputStream fromPlayer1;
    private DataOutputStream toPlayer1;
    private DataInputStream fromPlayer2;
    private DataOutputStream toPlayer2;

    // Continue to play
    private boolean continueToPlay = true;

    /** Construct a thread */
    public HandleASession(Socket player1, Socket player2) {
      this.player1 = player1;
      this.player2 = player2;

      // Initialize cells
      for (int i = 0; i < 20; i++)
        for (int j = 0; j < 20; j++)
          cell[i][j] = ' ';
    }

    /** Implement the run() method for the thread */
    public void run() {
      try {
        // Create data input and output streams
        DataInputStream fromPlayer1 = new DataInputStream(
          player1.getInputStream());
        DataOutputStream toPlayer1 = new DataOutputStream(
          player1.getOutputStream());
        DataInputStream fromPlayer2 = new DataInputStream(
          player2.getInputStream());
        DataOutputStream toPlayer2 = new DataOutputStream(
          player2.getOutputStream());

        // Write anything to notify player 1 to start
        // This is just to let player 1 know to start
        toPlayer1.writeInt(1);

        // Continuously serve the players and determine and report
        // the game status to the players
        //GAME LOGIC
        while (true) {
          // Receive a move from player 1
          int row = fromPlayer1.readInt();
          int column = fromPlayer1.readInt();
          cell[row][column] = 'X';

          // Check if Player 1 wins
          if (isWon('X')) {
            toPlayer1.writeInt(PLAYER1_WON);
            toPlayer2.writeInt(PLAYER1_WON);
            sendMove(toPlayer2, row, column);
            break; // Break the loop
          }
          else if (isFull()) { // Check if all cells are filled
            toPlayer1.writeInt(DRAW);
            toPlayer2.writeInt(DRAW);
            sendMove(toPlayer2, row, column);
            break;
          }
          else {
            // Notify player 2 to take the turn
            toPlayer2.writeInt(CONTINUE);

            // Send player 1's selected row and column to player 2
            sendMove(toPlayer2, row, column);
          }

          // Receive a move from Player 2
          row = fromPlayer2.readInt();
          column = fromPlayer2.readInt();
          cell[row][column] = 'O';

          // Check if Player 2 wins
          if (isWon('O')) {
            toPlayer1.writeInt(PLAYER2_WON);
            toPlayer2.writeInt(PLAYER2_WON);
            sendMove(toPlayer1, row, column);
            break;
          }
          else {
            // Notify player 1 to take the turn
            toPlayer1.writeInt(CONTINUE);

            // Send player 2's selected row and column to player 1
            sendMove(toPlayer1, row, column);
          }
        }
      }
      catch(IOException ex) {
        ex.printStackTrace();
      }
    }

    /** Send the move to other player */
    private void sendMove(DataOutputStream out, int row, int column)
        throws IOException {
      out.writeInt(row); // Send row index
      out.writeInt(column); // Send column index
    }

    /** Determine if the cells are all occupied */
    private boolean isFull() {
      for (int i = 0; i < 20; i++)
        for (int j = 0; j < 20; j++)
          if (cell[i][j] == ' ')
            return false; // At least one cell is not filled

      // All cells are filled
      return true;
    }

    /** Determine if the player with the specified token wins */
    private boolean isWon(char token) {
      // Check all rows
      for (int i = 0; i < 20; i++)
        if ((cell[i][0] == token)
            && (cell[i][1] == token)
            && (cell[i][2] == token)) {
          return true;
        }

      /** Check all columns */
      for (int j = 0; j < 20; j++)
        if ((cell[0][j] == token)
            && (cell[1][j] == token)
            && (cell[2][j] == token)) {
          return true;
        }

      /** Check major diagonal */
      if ((cell[0][0] == token)
          && (cell[1][1] == token)
          && (cell[2][2] == token)) {
        return true;
      }

      /** Check subdiagonal */
      if ((cell[0][2] == token)
          && (cell[1][1] == token)
          && (cell[2][0] == token)) {
        return true;
      }

      /** All checked, but no winner */
      return false;
    }
  }

  /**
   * The main method is only needed for the IDE with limited
   * JavaFX support. Not needed for running from the command line.
   */
  public static void main(String[] args) {
    Application.launch(args);
  }
  @Override
  public void stop(){
      System.out.println("Stage is closing");
      System.exit(0);
  }
}
