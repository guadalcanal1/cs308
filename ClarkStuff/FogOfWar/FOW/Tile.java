import com.sun.imageio.plugins.gif.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

public class Tile extends StackPane {
    // Indicate the row and column of this cell in the board

	private String type;
	private int dps;
	private int health;
	private boolean alive;

	private String color;
	private Color highlightColor;

	private int currentFrameIndex;
	ArrayList<Image> frames;

	private boolean volley;
	private boolean selected;
	private boolean highlighted;

    private int row;
    private int col;

    public Tile(int row, int col, String type) throws IOException {
      this.row = row;
      this.col = col;
      this.type = type;

      setWidth(FOWClient.TILE_SIZE);
	  setHeight(FOWClient.TILE_SIZE);

	  color = FOWClient.COLORS[FOWClient.clientColor];

	  if(new File("icons/"+type+".gif").exists()){
		  frames = getFrames(new File("icons/"+type+".gif"));
		}
	  currentFrameIndex = 0;
	  volley = false;

	  if(type.contains("commander")) {
			dps = 1;
			health = 150;
			alive = true;
	  } else if(type == "wall") {
		  	dps = 0;
		  	health = 100;
		  	alive = true;
	  } else if(type == "soldier") {
		  	dps = 0;
		  	health = 100;
		  	alive = true;
	  } else {
		  	dps = 0;
		  	health = 0;
		  	alive = false;
	  }
    }

    //Fix length = 1 issue

    public Image nextFrame() {
    	if(frames.size() == (currentFrameIndex + 1))
    		currentFrameIndex = 0;
    	Image hold = frames.get(currentFrameIndex);
    	if(frames.size() > (currentFrameIndex))
    		currentFrameIndex++;
    	return hold;
    }

    public void setVolley(boolean s) {
    	volley = s;
    }

    public String getType() {
    	return type;
    }

    public void setType(String t) {
    	type = t;
    }

    public void setHealth(int h) {
    	health = h;
    }

    public boolean isAlive() {
    	return alive;
    }

    public int getCurrentFrameIndex() {
    	return  currentFrameIndex;
    }
    public int getFrameSize() {
    	return frames.size();
    }


    public ArrayList<Image> getFrames(File gif) throws IOException{
        ArrayList<Image> frames = new ArrayList<Image>();
        ImageReader ir = new GIFImageReader(new GIFImageReaderSpi());
        ir.setInput(ImageIO.createImageInputStream(gif), false);
        for(int i = 0; i < ir.getNumImages(true); i++) {
            frames.add(SwingFXUtils.toFXImage(ir.read(i),null));
        }
        return frames;
    }


    private Color darker(Paint c) {
        return ((Color) c).darker().darker();
    }

    private Color brighter(Paint c) {
        return ((Color) c).brighter().brighter();
    }

	public void hoverUnhighlight() {
	}

	public void hoverHighlight() {
		/*Grid g = FOWClient.gameGrid;
		for (int r = 0; r < 20; r++) {
            for (int c = 0; c < 20; c++) {
            	if(g.getTile(r, c).isSelected()) {
            		if(FOWClient.moveCount >= (Math.abs(row - r)+Math.abs(col - c))) {
            			highlightColor = Color.rgb(42, 68, 214, 1);
            		} else {
            			highlightColor = Color.rgb(255, 0, 46, 1);
            		}
								int xTrans = 0;
								int yTrans = 0;
            		while (r != row && c != col){
									if(r > (row + xTrans)) {
										xTrans++;
									} else
									if(r < (row + xTrans)) {
										xTrans--;
									} else
									if(c > (col + yTrans)) {
										yTrans++;
									} else
									if(c > (col + yTrans)) {
										yTrans--;
									}
									FOWClient.gameGrid.getTile((row + xTrans), (col + yTrans)).highlight();
								}
            	}
            }
		}*/
	}

	public boolean isHighlighted() {
		return highlighted;
	}

	public void highlight() {
		highlighted = true;
	}

	public void unHighlight() {
		highlighted = false;
	}

	public Color getHColor(){
		return highlightColor;
	}

	public void select() {
		Grid g = FOWClient.gameGrid;
		for (int r = 0; r < 20; r++) {
            for (int c = 0; c < 20; c++) {
            	g.getTile(r, c).deselect();
            }
		}
		selected = true;
	}

	public void deselect() {
		selected = false;
	}

	public boolean isSelected() {
		return selected;
	}

}
