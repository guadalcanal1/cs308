import java.io.IOException;

import javafx.scene.layout.Pane;

public class Grid extends Pane {

        int rows;
        int columns;

        double width;
        double height;

        Tile[][] tiles;
        
        MouseGestures mg;

        public Grid( int rows, int columns, double width, double height, MouseGestures mg) {

            this.columns = columns;
            this.rows = rows;
            this.width = width;
            this.height = height;
            this.mg = mg;
            

            tiles = new Tile[rows][columns];

        }

        /**
         * Add tile to array and to the UI.
         */
        public void add(Tile tile, int row, int column) {

            tiles[row][column] = tile;

            double w = width / columns;
            double h = height / rows;
            double x = w * row;
            double y = h * column;

            tile.setLayoutX(x);
            tile.setLayoutY(y);
            tile.setPrefWidth(w);
            tile.setPrefHeight(h);

            tile.getStyleClass().add("gridTile");
            
            getChildren().add(tile);

        }

        public Tile getTile( int row, int column) {
            return tiles[row][column];
        }
        
        public void calcClears(int row, int col) throws IOException {
        	Tile hold = null;
        	for (int r = (row - 1); r < (row + 2); r++) {
                for (int c = (col - 1); c < (col + 2); c++) {
                	if( r >= 0 && c >= 0 && r < FOWClient.HEIGHT && c < FOWClient.WIDTH) {
                		if(tiles[r][c].getType() == "cloud_full") {
                			//System.out.println(" ("+r+","+c+") ");
                			hold = new Tile(r,c,"clear");
                	    	mg.makePaintable(hold);
                			tiles[r][c] = hold;
                		}
                	}
                }
        	}
        }
        
    }