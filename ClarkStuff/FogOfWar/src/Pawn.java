

public class Pawn{
	private int x;
	private int y;
	private String color;
	Pawn(int y, int x, String type){
		
		this.x = x;
		this.y = y;
		if(type == "commander")
			color = FOWClient.COLORS[FOWClient.clientColor];
	}
	
	public int[] getCordinates() {
		int[] cord = new int[2];
		cord[0] = x;
		cord[1]= y;
		return cord;
	}
	public String getColor() {
		return color;
	}
}
