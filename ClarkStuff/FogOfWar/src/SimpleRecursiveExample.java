
public class SimpleRecursiveExample {

	
	  public static void main(String[] args) {
		  System.out.println(H(4));
	}
	
	public static int p(int n) {
		if (n > 0)
			return (n - 1) + p(n-1);
		else
			return 0;
	}
	

	private class F{
		
		private int b;
		private String bTest;
		private String fEx;
		private String fTest;
		
		
		public F(int base, String baseTest, String functionEx, String functionTest) {
			b = base;
			bTest = baseTest;
			fEx = functionEx;
			fTest = functionTest;
		}
		
		private void initTest() {
			
		}
	}
	
	/*
	 * 		{ 1				, n = 1 or n = 2
	 * F(n)=|
	 * 		{ F(n-1)+F(n-2) , n > 2
	 */
	public static int Fib(int n) {
		if (n > 2)
			return Fib(n-1) + Fib(n-2);
		else if (n == 1 || n == 2)
			return 1;
		return 0;
	}

	
	public static double loadCalc(double amt, int w, boolean rec) {
		if(!rec) {
			double r = .1;
			for (int i = 0; i < w; i++) 
				amt = amt + amt * r;
			return amt;
		} else {
			if (w > 0)
				return 1.1 * loadCalc(amt, w - 1, rec);
			else
				return amt;
		}
	}
	public static int H(int n) {
		if (n > 1)
			return H(n - 1) + (6*n) - 6;
		if (n == 1)
			return 0;
		return 0;
	}
}