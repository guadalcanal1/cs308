

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fow.handlers.User;
import com.fow.handlers.requestUser;

import javafx.animation.AnimationTimer;
import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.sql.Driver;
import java.util.concurrent.CountDownLatch;
/* How a game will operate:
 * A player will roll dice(1-6),
 * The client will send the dice number,
 * server will respond with the other players pawn positions,
 * If any of the other player's gunner or commander pawns are near the dice rolling player,
 * they will "shoot" enemy pawn near them. Once the pawns have battled it out,
 * the client will send the results back to the server. The server will record the data in the DB.
 * If the commander is dead, the game is over.
 * 
 * the client will click on a pawn and then click on a tile,
 * the client will check the distance and the amount of moves and either complete the move
 * or notify the player. The client will also look to see if the other players pawns are in the way.
 * This will loop until all the moves have been used up.
 * 
 * Things to add:
 *  - use INI file instead of txt to store local data
 * 
*/
public class FOWClient extends Application
    implements TicTacToeConstants {
  public static final CountDownLatch latch = new CountDownLatch(1);
  public static FOWClient FOWclient = null;

   public static FOWClient waitForClient() {
       try {
           latch.await();
       } catch (InterruptedException e) {
           e.printStackTrace();
       }
       return FOWclient;
   }

   public static void setFOWClient(FOWClient FOWClient0) {
     FOWclient = FOWClient0;
     latch.countDown();
   }

   public FOWClient() {
     setFOWClient(this);
   }

  private User user;

  private Stage globalStage;
  private Scene startScene;
  private Scene loadingScene;
  private Scene nameCreationScene;
  private Scene gameBrowserScene;
  private Scene gameScene;


  //Game Browser Table
  private TableView<User> table = new TableView<User>();
  private final ObservableList<User> data = FXCollections.observableArrayList();

  // Indicate whether the player has the turn
  private boolean myTurn = false;

  public GraphicsContext gc;

  // Create and initialize cells
  public static final int TILE_SIZE = 35;
  public static final int WIDTH = 20;
  public static final int HEIGHT = 20;

  public static final String[] COLORS = {"red","blue","green","white"};
  public static int clientColor;
  public static int moveCount;

  // Create and initialize a title label
  public static Grid gameGrid;
  private Label lblTitle = new Label();
  private Image logo = new Image("icons/Game_Logo.png", true);
  private Image startIcon = new Image("icons/StartButton.png", true);

  // Create and initialize a status label
  private Label lblStatus = new Label();

  // Indicate selected row and column by the current move
  private int rowSelected;
  private int columnSelected;

  // Input and output streams from/to server
  private DataInputStream fromServer;
  private DataOutputStream toServer;
  private ObjectOutputStream objectToServer;

  // Continue to play?
  private boolean continueToPlay = true;

  // Wait for the player to mark a cell
  private boolean waiting = true;

  // Host name or ip
  private String host = "127.0.0.1";

  @Override // Override the start method in the Application class
  public void start(Stage primaryStage) {
	globalStage = primaryStage;
    //can now use the stage in other methods

	//create scenes and panes
    BorderPane startPane = new BorderPane();

    VBox buttonAndLogo = new VBox();
    buttonAndLogo.setPadding(new Insets(10));
    buttonAndLogo.setSpacing(8);
    Pane gameBrowserPane = new Pane();
    GridPane selectedGameInfo = new GridPane();
    VBox tableVbox = new VBox();
    Label gameInfo = new Label();

    gameBrowserScene = new Scene(gameBrowserPane,500,500);
    gameBrowserScene.getStylesheets().add(getClass().getResource("client.css").toExternalForm());


    ImageView logoView = new ImageView();
    ImageView startButtonView = new ImageView();

	startScene = new Scene(startPane,500,500);
	startScene.getStylesheets().add(getClass().getResource("client.css").toExternalForm());

    VBox nameCreationPane = new VBox();
    nameCreationScene = new Scene(nameCreationPane, 500, 500);
    nameCreationScene.getStylesheets().add(getClass().getResource("client.css").toExternalForm());

    FadeTransition ftSP = new FadeTransition(Duration.millis(1000), startPane);
    ftSP.setFromValue(1.0);
    ftSP.setToValue(0);
    ftSP.setCycleCount(1);

    FadeTransition ftCN = new FadeTransition(Duration.millis(1000), nameCreationPane);
    ftCN.setFromValue(1.0);
    ftCN.setToValue(0);
    ftCN.setCycleCount(1);


    //Name Creation Scene
    //

    Label nameCreationLabel = new Label("Create A Name");
    nameCreationLabel.getStyleClass().add("startText");
    TextField enteredName = new TextField();
    enteredName.setPromptText("Enter your name.");
    enteredName.setPrefColumnCount(10);
    enteredName.setOnKeyPressed(new EventHandler<KeyEvent>() {
        @Override
        public void handle(KeyEvent keyEvent) {
            if (keyEvent.getCode() == KeyCode.ENTER)  {
                String text = enteredName.getText();
                text = scrubText(text);
                if(!isNameinDB(text)) {
	                BufferedWriter bw = null;
	        		FileWriter fw = null;
	                try {
	                	fw = new FileWriter("name.txt");
	        			bw = new BufferedWriter(fw);
	        			bw.write(text);
	        			createName(text);
	        			System.out.println("Done");
	        			ftCN.setOnFinished(f -> {
	        				loadGameBrowserScene();
	        			});
	        			ftCN.play();
	        		} catch (IOException e) {
	        			e.printStackTrace();
	        		} finally {
	        			try {
	        				if (bw != null)
	        					bw.close();

	        				if (fw != null)
	        					fw.close();

	        			} catch (IOException ex) {
	        				ex.printStackTrace();
	        			}

	        		}
                }
                else {
                	nameCreationLabel.setWrapText(true);
                	nameCreationLabel.setText("Try a different name, that one is already taken");
                }
            }
        }
    });
    nameCreationPane.getChildren().addAll(nameCreationLabel,enteredName);
    nameCreationPane.setAlignment(Pos.CENTER);
    nameCreationPane.getStyleClass().add("defaultPane");


    //
    //

    //Loading Scene
    //

    //loadingScene.getStylesheets().add(getClass().getResource("client.css").toExternalForm());

    //
    //

    //Game Browser Scene
    //

    Label label = new Label("Game Browser");

    table.setEditable(false);

    TableColumn nameCol = new TableColumn("Name");
    nameCol.setMinWidth(200);
    nameCol.setCellValueFactory(
            new PropertyValueFactory<User, String>("Name"));

    TableColumn statusCol = new TableColumn("Status");
    statusCol.setMinWidth(200);
    statusCol.setCellValueFactory(
            new PropertyValueFactory<User, String>("Status"));

    //adds on click event handler

    table.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
        if (newSelection != null) {
        	User selectedUser = table.getSelectionModel().getSelectedItem();
        	gameInfo.setText("Start game with "+selectedUser.getName());
        	gameInfo.setOnMouseClicked(new EventHandler<MouseEvent>() {
        	      @Override
        	      public void handle(MouseEvent e) {
        	    	try {
        	    		System.out.println("Start Game!");
						objectToServer.writeObject(new requestUser(100, selectedUser));
						gameScene = new Scene(initGame());
						globalStage.setScene(gameScene);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
        	      }
        	});
        	System.out.println(selectedUser.getName());
        }
    });

    table.setItems(data);
    table.getColumns().addAll(nameCol, statusCol);
    table.getStyleClass().add("gameBrowserTable");

    tableVbox.setSpacing(5);
    tableVbox.setPadding(new Insets(0, 0, 0, 10));

    selectedGameInfo.add(gameInfo, 1, 0);
    selectedGameInfo.setAlignment(Pos.BOTTOM_CENTER);

    tableVbox.getChildren().addAll(label, table, selectedGameInfo);
    gameBrowserPane.getChildren().addAll(tableVbox);

    //
    //
    //Start Scene
    //

    //add logo, resize and add to pane
    logoView.setImage(logo);
    logoView.setFitHeight(200);
    logoView.setFitWidth(200);
    logoView.setPreserveRatio(true);
    buttonAndLogo.getChildren().add(logoView);

    //add button and style

    startButtonView.setImage(startIcon);
    startButtonView.setFitHeight(100);
    startButtonView.setFitWidth(100);
    startButtonView.setPreserveRatio(true);

    Text startText = new Text("Start!");
    startText.getStyleClass().add("startText");
    StackPane StartButtonPane = new StackPane();

    StartButtonPane.getChildren().add(startButtonView);
    StartButtonPane.getChildren().add(startText);

    StartButtonPane.setPickOnBounds(true); // allows click on transparent areas
    StartButtonPane.setOnMouseClicked((MouseEvent e) -> {
        ftSP.setOnFinished(f -> {
			try {
				checklocalName();
			    connectToServer();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		});
        ftSP.play();
    });

    buttonAndLogo.getChildren().add(StartButtonPane);
    buttonAndLogo.setAlignment(Pos.CENTER);

    startPane.setCenter(buttonAndLogo);
    startPane.getStyleClass().add("defaultPane");

    //
    //

    // place scene in the stage
    primaryStage.getIcons().add(
    		   new Image(
    				   FOWClient.class.getResourceAsStream( "icons/Game_Logo.png" )));
    primaryStage.setTitle("Fog of War"); // Set the stage title
    primaryStage.setScene(startScene); // Place the scene in the stage
    primaryStage.show(); // Display the stage

  }

  public static void updateUserData(String col, String newData, int userid) {
	  col.replaceAll("[^a-zA-Z&&^-]", "").toLowerCase();
	  newData.replaceAll("[^a-zA-Z&&^-]", "").toLowerCase();
      try {
      Class.forName("com.mysql.jdbc.Driver");
      Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/FogOFWar", "root", "");
      Statement smt = con.createStatement(); //Create Statement to interact
      smt.executeUpdate("UPDATE `users` SET `" + col + "` = '" + newData + "' WHERE `UserId` = " + userid);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  private void checklocalName() throws IOException {
	BufferedReader br;
	try {
      br = new BufferedReader(new FileReader("name.txt"));
	  String line;
	  boolean nameFound = false;
	  line = br.readLine();
	  if ((line != null) && (line != "")) {
		    nameFound = true;
	  }

	  if (!nameFound) {
		  globalStage.setScene(nameCreationScene);
	  } else if(isNameinDB(line)) {
		  user = getDBUser(line);
		  loadGameBrowserScene();
	  }
	  br.close();
	  } catch (FileNotFoundException e) {
		  System.out.println("FILE NOT FOUND!");
		  PrintWriter writer = new PrintWriter("name.txt", "UTF-8");
		  writer.close();
		  globalStage.setScene(nameCreationScene);
	  }
  }

  private boolean isNameinDB(String text) {
	      try {Class.forName("com.mysql.jdbc.Driver");
	        Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/FogOFWar", "root", "");
	        Statement smt = con.createStatement(); //Create Statement to interact
	        ResultSet r = smt.executeQuery("select * from Users;");
	        text = scrubText(text);
	        while (r.next()) {
	        	if(r.getString("name").equalsIgnoreCase(text)) {
	        		return true;
	        	}
	        }
	      }
	      catch (Exception ex) {
	        ex.printStackTrace();
	      }
	  return false;
  }
  private User getDBUser(String text) {
      try {Class.forName("com.mysql.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/FogOFWar", "root", "");
        Statement smt = con.createStatement(); //Create Statement to interact
        ResultSet r = smt.executeQuery("select * from Users;");
        text = scrubText(text);
        System.out.println(text);
        while (r.next()) {
        	if(r.getString("name").equalsIgnoreCase(text)) {
        		return new User(r.getString("name"),r.getInt("UserId"),r.getString("state"));
        	}
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
      return null;
}

  private void createName(String text) {
	  text = scrubText(text);
	  try {Class.forName("com.mysql.jdbc.Driver");
      Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/FogOFWar", "root", "");
      Statement smt = con.createStatement(); //Create Statement to interact
      //String query = "INSERT INTO `users`(`name`, `state`) VALUES (" + text + ",In-Game)";
      //System.out.println(text);
      smt.executeUpdate("INSERT INTO `users`(`name`,`state`) VALUES ('"+ text +"','In-Game')");
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  @SuppressWarnings("resource")
private void loadGameBrowserScene() {
	  globalStage.setScene(gameBrowserScene);
	  new Thread(() -> {
	      try {Class.forName("com.mysql.jdbc.Driver");
	        Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/FogOFWar", "root", "");
	        Statement smt = con.createStatement(); //Create Statement to interact
	        BufferedReader br;
	          br = new BufferedReader(new FileReader("name.txt"));
	    	  String name = br.readLine();
	    	  name = scrubText(name);
	        ResultSet r = smt.executeQuery("select * from Users where `name` != '"+name+"';");
	        while (r.next()) {
	          data.add(new User(r.getString("name"),r.getInt("UserID"), r.getString("state")));
	        }
	      }
	      catch (Exception ex) {
	        ex.printStackTrace();
	      }
	 }).start();
  }

  public Group initGame() throws IOException {
	  Group root = new Group();

	  BorderPane pane = new BorderPane();
	  pane.setPrefWidth(WIDTH * TILE_SIZE);
	  pane.setPrefHeight((HEIGHT + 3) * TILE_SIZE);

	    // Create a wrapper Pane first
	    StackPane wrapperPane = new StackPane();
	    pane.setCenter(wrapperPane);
	    wrapperPane.setPrefWidth(WIDTH * TILE_SIZE);
	    wrapperPane.setPrefHeight(HEIGHT * TILE_SIZE);
	    // Put canvas in the center of the window
	    Canvas canvas = new Canvas();

	    MouseGestures mg = new MouseGestures();

	    gameGrid = new Grid(20, 20, WIDTH * TILE_SIZE, HEIGHT * TILE_SIZE, mg);

	    gc = canvas.getGraphicsContext2D();


        // fill gameGrid

        for (int r = 0; r < 20; r++) {
            for (int c = 0; c < 20; c++) {
            	Tile tile = null;
            	tile = new Tile(r, c, "cloud_full");
            	mg.makePaintable(tile);
                gameGrid.add(tile, r, c);
                gameGrid.getTile(r, c).deselect();
                gameGrid.getTile(r, c).unHighlight();
            }
        }

        //Instantiate Commander
	    Tile hold = new Tile(19,9,"commander_red");
	    mg.makePaintable(hold);
        gameGrid.add(hold,19,9);

        //Instantiate Soldiers
        hold = new Tile(19,7,"soldier");
        mg.makePaintable(hold);
        gameGrid.add(hold,19,7);
        hold = new Tile(19,11,"soldier");
        mg.makePaintable(hold);
        gameGrid.add(hold,19,11);
        hold = new Tile(18,8,"soldier");
        mg.makePaintable(hold);
        gameGrid.add(hold,18,8);
        hold = new Tile(18,10,"soldier");
        mg.makePaintable(hold);
        gameGrid.add(hold,18,10);
        hold = new Tile(17,9,"soldier");
        mg.makePaintable(hold);
        gameGrid.add(hold,17,9);

        //Instantiate walls
        hold = new Tile(19,6,"wall");
        mg.makePaintable(hold);
        gameGrid.add(hold,19,6);
        hold = new Tile(19,12,"wall");
        mg.makePaintable(hold);
        gameGrid.add(hold,19,12);
        hold = new Tile(18,7,"wall");
        mg.makePaintable(hold);
        gameGrid.add(hold,18,7);
        hold = new Tile(18,11,"wall");
        mg.makePaintable(hold);
        gameGrid.add(hold,18,11);
        hold = new Tile(17,8,"wall");
        mg.makePaintable(hold);
        gameGrid.add(hold,17,8);
        hold = new Tile(17,10,"wall");
        mg.makePaintable(hold);
        gameGrid.add(hold,17,10);
        hold = new Tile(16,9,"wall");
        mg.makePaintable(hold);
        gameGrid.add(hold,16,9);


        //initialize move count
        moveCount = 6;

	    wrapperPane.getChildren().addAll(gameGrid,canvas);
	    gameGrid.toFront();
	    // Bind the width/height property to the wrapper Pane
	    canvas.widthProperty().bind(wrapperPane.widthProperty());
	    canvas.heightProperty().bind(wrapperPane.heightProperty());
	    lblTitle.setText("Fog Of War");
	    pane.setTop(lblTitle);
	    pane.setCenter(wrapperPane);

	    pane.setBottom(lblStatus);

	  root.getChildren().add(pane);

	  gameLoop();

	  return root;
  }

  private void gameLoop(){

	    new AnimationTimer() {

	        public void handle(long currentNanoTime) {

	        	//Clear the canvas
                gc.setFill( Color.WHITE );
                gc.fillRect(0,0, WIDTH * TILE_SIZE, HEIGHT * TILE_SIZE);

                try {
	                for (int r = 0; r < 20; r++) {
	                    for (int c = 0; c < 20; c++) {
	                    	if(gameGrid.getTile(r,c).isAlive()) {
								gameGrid.calcClears(r,c);
	                    	}
	                    }
	                }
                } catch (IOException e) {
					e.printStackTrace();
				}

                for (int r = 0; r < 20; r++) {
                    for (int c = 0; c < 20; c++) {
                    	// draw tiles
                		if (gameGrid.getTile(r,c).getType() != "clear")
	                		gc.drawImage(gameGrid.getTile(r,c).nextFrame(),
	                				c * TILE_SIZE, r * TILE_SIZE, TILE_SIZE, TILE_SIZE);

                    	//	draw Grid border
                		gc.setFill(Color.rgb(255, 0, 46, 0.5));
                		if(gameGrid.getTile(c,r).isSelected()) {
                			gc.setStroke(Color.rgb(255, 0, 46, 1));
    	                	gc.fillRect(c * TILE_SIZE, r * TILE_SIZE, TILE_SIZE, TILE_SIZE);
                    	} else if(gameGrid.getTile(c,r).isHighlighted()) {
                			gc.setFill(gameGrid.getTile(c,r).getHColor());
    	                	gc.fillRect(c * TILE_SIZE, r * TILE_SIZE, TILE_SIZE, TILE_SIZE);
                    	} else {
                    		gc.setStroke(Color.rgb(0, 0, 0, 1));
                    	}
                		gc.strokeRect(c * TILE_SIZE, r * TILE_SIZE, TILE_SIZE, TILE_SIZE);

	                }
	            }
	        }
	    }.start();
  }


private void connectToServer() {
    try {
      // Create a socket to connect to the server
      Socket socket = new Socket(host, 8000);

      // Create an input stream to receive data from the server
      fromServer = new DataInputStream(socket.getInputStream());

      // Create an output stream to send data to the server
      OutputStream serverOutputStream = socket.getOutputStream();
      toServer = new DataOutputStream(serverOutputStream);

      objectToServer = new ObjectOutputStream(serverOutputStream);
      System.out.println(objectToServer.toString());
      try {
    	int serverResponse = fromServer.readInt();
		if(serverResponse == 1)
			  System.out.println(serverResponse);
			  objectToServer.writeObject(user);
		} catch (IOException e) {
			e.printStackTrace();
		}

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }

    /*// Control the game on a separate thread
    new Thread(() -> {
      try {
        // Get notification from the server
        int player = fromServer.readInt();

        // Am I player 1 or 2?
        if (player == 1) {
          myToken = 'X';
          otherToken = 'O';
          Platform.runLater(() -> {
            lblTitle.setText("Player 1 with token 'X'");
            lblStatus.setText("Waiting for player 2 to join");
          });

          // Receive startup notification from the server
          fromServer.readInt(); // Whatever read is ignored

          // The other player has joined
          Platform.runLater(() ->
            lblStatus.setText("Player 2 has joined. I start first"));

          // It is my turn
          myTurn = true;
        }
        else if (player == 2) {
          myToken = 'O';
          otherToken = 'X';
          Platform.runLater(() -> {
            lblTitle.setText("Player 2 with token 'O'");
            lblStatus.setText("Waiting for player 1 to move");
          });
        }

        // Continue to play
        while (continueToPlay) {
          if (player == PLAYER1) {
            waitForPlayerAction(); // Wait for player 1 to move
            sendMove(); // Send the move to the server
            receiveInfoFromServer(); // Receive info from the server
          }
          else if (player == PLAYER2) {
            receiveInfoFromServer(); // Receive info from the server
            waitForPlayerAction(); // Wait for player 2 to move
            sendMove(); // Send player 2's move to the server
          }
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
    }).start();*/
  }

  /** Wait for the player to mark a cell
  private void waitForPlayerAction() throws InterruptedException {
    while (waiting) {
      Thread.sleep(100);
    }

    waiting = true;
  }

  // Send this player's move to the server
  //Instead should be: (type, cordinate)
  private void sendMove() throws IOException {
    toServer.writeInt(rowSelected); // Send the selected row
    toServer.writeInt(columnSelected); // Send the selected column
  }

  // Receive info from the server
  private void receiveInfoFromServer() throws IOException {
    // Receive game status
    int status = fromServer.readInt();

    if (status == PLAYER1_WON) {
      // Player 1 won, stop playing
      continueToPlay = false;
      if (myToken == 'X') {
        Platform.runLater(() -> lblStatus.setText("I won! (X)"));
      }
      else if (myToken == 'O') {
        Platform.runLater(() ->
          lblStatus.setText("Player 1 (X) has won!"));
        receiveMove();
      }
    }
    else if (status == PLAYER2_WON) {
      // Player 2 won, stop playing
      continueToPlay = false;
      if (myToken == 'O') {
        Platform.runLater(() -> lblStatus.setText("I won! (O)"));
      }
      else if (myToken == 'X') {
        Platform.runLater(() ->
          lblStatus.setText("Player 2 (O) has won!"));
        receiveMove();
      }
    }
    else if (status == DRAW) {
      // No winner, game is over
      continueToPlay = false;
      Platform.runLater(() ->
        lblStatus.setText("Game is over, no winner!"));

      if (myToken == 'O') {
        receiveMove();
      }
    }
    else {
      receiveMove();
      Platform.runLater(() -> lblStatus.setText("My turn"));
      myTurn = true; // It is my turn
    }
  }

  private void receiveMove() throws IOException {
    // Get the other player's move
    int row = fromServer.readInt();
    int column = fromServer.readInt();
    Platform.runLater(() -> tiles[row][column].setToken(otherToken));
  }
  */
  public String scrubText(String dirty) {
		dirty = scrub("\\s+", dirty, " ");
		return scrub("^\\s?|\\s?$", dirty, "");
	}

  public String scrub(String pattern, String text, String replace) {
		Pattern p = Pattern.compile(pattern);
		Matcher matcher = p.matcher(text);
		return matcher.replaceAll(replace);
	}

  public static void main(String[] args) {
	    launch(args);
	  }
    @Override
    public void stop(){
        System.out.println("Stage is closing");
        updateUserData("state","offline",user.getID());
        System.exit(0);
    }
}
