CREATE TABLE `gotcha_players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) DEFAULT NULL,
  `middle_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `is_friendly` tinyint(1) DEFAULT 0,
  `is_alive` tinyint(1) DEFAULT 1,
  `target_id` INT(11) DEFAULT 0,
  `targeted_by_id` INT(11) DEFAULT 0,
  `group_id` INT(11) DEFAULT 0,
  `props` TEXT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `kills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `killer_id` INT(11) DEFAULT 0,
  `killed_id` INT(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `player_names` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` INT(11) DEFAULT 0,
  `name_count` INT(11) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `player_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` INT(11) DEFAULT 0,
  `comment` TEXT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(64),
  `password` VARCHAR(512),
  `email_address` VARCHAR(64),
  `group_id` INT(11) DEFAULT 0,
  `player_id` INT(11) DEFAULT 0,
  `is_admin` tinyint(1) DEFAULT 0,
  `auth_token` varchar(512 ) DEFAULT NULL,
  `salt` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(128),
  `password` VARCHAR(128),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `group_admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11),
  `group_id` int(11),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `group_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11),
  `group_id` int(11),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `groups`(`id`, `name`, `password`) values(1, 'TMU', 'password');
INSERT INTO `users`(`id`, `username`, `password`, `email_address`, `group_id`, `player_id`, `is_admin`) values(1, 'AdminUser', 'password', "test1@email.com", 1, 1, 1);
INSERT INTO `users`(`id`, `username`, `password`, `email_address`, `group_id`, `player_id`, `is_admin`) values(2, 'RegularUser', 'password', "test2@email.com", 1, 2, 0);
INSERT INTO `gotcha_players`(`id`, `first_name`, `middle_name`, `last_name`, `is_friendly`, `is_alive`, `target_id`, `targeted_by_id`,`group_id`)
VALUES(1, 'Bob', 'P', 'Nugget', 1, 1, 3, 4, 1);
INSERT INTO `gotcha_players`(`id`, `first_name`, `middle_name`, `last_name`, `is_friendly`, `is_alive`, `target_id`, `targeted_by_id`,`group_id`)
VALUES(2, 'Jim', 'B', 'Potato', 1, 1, 4, 3, 1);

INSERT INTO `gotcha_players`(`id`, `first_name`, `middle_name`, `last_name`, `is_friendly`, `is_alive`, `target_id`, `targeted_by_id`,`group_id`)
VALUES(3, 'Badguy', 'A', 'Generic', 0, 1, 2, 1, 1);
INSERT INTO `gotcha_players`(`id`, `first_name`, `middle_name`, `last_name`, `is_friendly`, `is_alive`, `target_id`, `targeted_by_id`,`group_id`)
VALUES(4, 'Baddude', 'B', 'Typical', 0, 1, 1, 2, 1);

INSERT INTO `gotcha_players`(`id`, `first_name`, `middle_name`, `last_name`, `is_friendly`, `is_alive`, `target_id`, `targeted_by_id`,`group_id`)
VALUES(5, 'Chain', 'A', '1', 1, 1, 6, 0, 1);
INSERT INTO `gotcha_players`(`id`, `first_name`, `middle_name`, `last_name`, `is_friendly`, `is_alive`, `target_id`, `targeted_by_id`,`group_id`)
VALUES(6, 'Chain', 'B', '2', 1, 1, 7, 6, 1);
INSERT INTO `gotcha_players`(`id`, `first_name`, `middle_name`, `last_name`, `is_friendly`, `is_alive`, `target_id`, `targeted_by_id`,`group_id`)
VALUES(7, 'Chain', 'A', '3', 1, 1, 8, 7, 1);
INSERT INTO `gotcha_players`(`id`, `first_name`, `middle_name`, `last_name`, `is_friendly`, `is_alive`, `target_id`, `targeted_by_id`,`group_id`)
VALUES(8, 'Chain', 'B', '4', 1, 1, 0, 8, 1);

INSERT INTO `player_names`(`player_id`, `name_count`) VALUES(1, 1);
INSERT INTO `player_names`(`player_id`, `name_count`) VALUES(2, 5);
INSERT INTO `player_names`(`player_id`, `name_count`) VALUES(3, 2);
INSERT INTO `player_names`(`player_id`, `name_count`) VALUES(4, 2);