<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<!-- Header -->
<header id="head">
	<div class="container">
		<div class="row">
			<h1 class="lead">${messages.get("title")}</h1>
			<p class="tagline">
				${messages.get("description")} <a
					href="https://htmlhigh5.com">HTML High 5</a>
			</p>
			<p>
				<a class="btn btn-default btn-lg" role="button">${messages.get("signup")}</a> <a
					class="btn btn-action btn-lg" role="button">${messages.get("login")}</a>
			</p>
		</div>
	</div>
</header>
<!-- /Header -->

<!-- container -->
<div class="container">


	<div class="row col-md-12">
		<div class="col-md-4">
		<h1>${messages.get("avoid") }</h1>
			<c:forEach items="${targeters}" var="s">
				<%@include file="./fragments/playerCard.jspf" %>
			</c:forEach>
		</div>
		<div class="col-md-4">
		<h1>${messages.get("ourPeople") }</h1>
			<c:forEach items="${friends}" var="s">
				<%@include file="./fragments/playerCard.jspf" %>
			</c:forEach>
		</div>
		<div class="col-md-4">
		<h1>${messages.get("targets") }</h1>
			<c:forEach items="${targets}" var="s">
				<%@include file="./fragments/playerCard.jspf" %>
			</c:forEach>
		</div>
	</div>
	<div class="row col-md-12">
		<h1>${messages.get("chains") }</h1>
		<c:forEach items="${chains}" var="chain">
			<div class="col-md-12">
				<h2>${messages.get("length") }: <span>${chain.getLength()}</span></h2>
				<h2>${messages.get("names") }: <span>${chain.getNames()}</span></h2>
				<c:forEach items="${chain.getPlayers()}" var="s">
					<c:if test="${!chain.isFirst(s)}">
					    <div class="leftLink"></div>
				    </c:if>
					<div class="chainLink">${s.fullName()}</div>
					<c:if test="${!chain.isLast(s)}">
					    <div class="rightLink"></div>
				    </c:if>
				</c:forEach>
			</div>
		</c:forEach>
	</div>

</div>
<!-- /container -->

<!-- Social links. @TODO: replace by link/instructions in template -->
<section id="social">
	<div class="container">
		<div class="wrapper clearfix">
			<!-- AddThis Button BEGIN -->
			<div class="addthis_toolbox addthis_default_style">
				<a class="addthis_button_facebook_like"
					fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a>
				<a class="addthis_button_linkedin_counter"></a> <a
					class="addthis_button_google_plusone" g:plusone:size="medium"></a>
			</div>
			<!-- AddThis Button END -->
		</div>
	</div>
</section>
<!-- /social links -->


