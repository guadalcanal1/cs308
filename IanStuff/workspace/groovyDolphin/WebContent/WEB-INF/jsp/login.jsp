<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<div style="margin-top: 100px" class="formContainer loginContainer">
	<h1 class="formTitle">Log In</h1>
	<form id="loginForm" method="POST" action="/login">
	
		<label>Username</label>
		<input type="text" name="username">
		<br/>
		<label>Password</label>
		<input type="password" name="password">
		<br/>
		<input type="submit" class="button" value="Log In">
	</form>
</div>