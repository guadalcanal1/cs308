<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<div style="margin-top: 100px" class="formContainer registerContainer">
	<h1 class="formTitle">Register</h1>
	<form id="registerForm" method="POST" action="/register">
	
		<label>Username</label>
		<input type="text" name="username">
		<br/>
		<label>Password</label>
		<input type="password" name="password">
		<br/>
		<label>Repeat Password</label>
		<input type="password" name="password2">
		<br/>
		<label>Email Address</label>
		<input type="email" name="email">
		<br/>
		<input type="submit" class="button" value="Register">
	</form>
</div>