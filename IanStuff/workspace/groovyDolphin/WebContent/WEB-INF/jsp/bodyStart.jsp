<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<body>
<!-- Fixed navbar -->
	<div class="navbar navbar-inverse navbar-fixed-top headroom" >
		<div class="container">
			<div class="navbar-header">
				<!-- Button for smallest screens -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<a class="navbar-brand" href="index.html"><img src="assets/images/logo.png" alt="Progressus HTML5 template"></a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav pull-right">
					<li class="active"><a href="/home">${messages.get("home") }</a></li>
					<li><a href="contact.html">${messages.get("contact") }</a></li>
					<c:choose>
						<c:when test="${user == null }">
							<li><a class="btn" href="/login">${messages.get("login") }</a></li>
							<li><a class="btn" href="/register">${messages.get("signup") }</a></li>
						</c:when>
						<c:otherwise>
							<li><a href="/me">${user.str("username") }</a></li>
							<li><a href="/groups">${messages.get("myGroups") }</a></li>
							<li><a href="/createGroup">${messages.get("createGroup") }</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">More Pages <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a href="sidebar-left.html">Left Sidebar</a></li>
									<li class="active"><a href="sidebar-right.html">Right Sidebar</a></li>
								</ul>
							</li>
							<li><form id="logout" action="/logout" method="POST">
								<input type="submit" value='${messages.get("logOut") }' class="btn">
							</form></li>
						</c:otherwise>
					</c:choose>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</div> 
	<!-- /.navbar -->