<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<div style="margin-top: 100px; overflow: auto">
	<h1 class="groupsTitle">Your Groups</h1>
	<div class="col-md-12">
		<h2>Groups You Administer</h2>
		<c:forEach items="${adminGroups}" var="s">
			<%@include file="./fragments/groupCard.jspf" %>
		</c:forEach>
	</div>
	<div class="col-md-12">
		<h2>Groups you are in</h2>
		<c:forEach items="${memberGroups}" var="s">
			<%@include file="./fragments/groupCard.jspf" %>
		</c:forEach>
	</div>
</div>