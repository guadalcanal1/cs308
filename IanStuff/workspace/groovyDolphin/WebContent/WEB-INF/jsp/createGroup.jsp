<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div style="margin-top: 100px" class="formContainer createGroupContainer">
	<h1 class="formTitle">Log In</h1>
	<form id="addGroupForm" method="POST" action="/createGroup">
	
		<label>Group Name</label>
		<input type="text" name="name">
		<br/>
		<label>Password</label>
		<input type="password" name="password">
		<br/>
		<label>Repeat Password</label>
		<input type="password" name="password2">
		<br/>
		<input type="submit" class="button" value="Create Group">
	</form>
</div>