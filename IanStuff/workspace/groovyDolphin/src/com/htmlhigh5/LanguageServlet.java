package com.htmlhigh5;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.htmlhigh5.model.User;

public class LanguageServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String language, country;
		String[] params = request.getQueryString().split("&");
		language = params[0].split("=")[1];
		country = params[1].split("=")[1];
		response.addCookie(new Cookie("language", language));
		response.addCookie(new Cookie("country", country));
		response.sendRedirect("/");
	}
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
	}
}