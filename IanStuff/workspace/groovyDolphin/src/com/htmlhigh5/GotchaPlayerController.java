package com.htmlhigh5;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.TreeSet;

import com.htmlhigh5.model.GotchaPlayer;

public class GotchaPlayerController {
	
	public static ArrayList<GotchaPlayer> getFriends(int groupID) {
		return GotchaPlayer.where(new String[] { "is_friendly", "group_id" }, new String[] { "=", "=" },
				new Object[] { 1, groupID });
	}
	
	public static TreeSet<GotchaPlayer> getTargeters(int groupID){
		ArrayList<GotchaPlayer> friends = GotchaPlayerController.getFriends(groupID);
		TreeSet<GotchaPlayer> targeters = new TreeSet<GotchaPlayer>();
		for(GotchaPlayer p : friends)
			if(p.targetedBy() != null)
				targeters.add(p.targetedBy());
		return targeters;
	}
	
	public static TreeSet<GotchaPlayer> getTargets(int groupID){
		ArrayList<GotchaPlayer> friends = GotchaPlayerController.getFriends(groupID);
		TreeSet<GotchaPlayer> targets = new TreeSet<GotchaPlayer>();
		for(GotchaPlayer p : friends)
			if(p.target() != null)
				targets.add(p.target());
		return targets;
	}
	
	public static ArrayList<Chain> getChains(int groupID){
		ArrayList<Chain> chains = new ArrayList<Chain>();
		ArrayList<GotchaPlayer> friends = GotchaPlayerController.getFriends(groupID);
		HashSet<GotchaPlayer> checked = new HashSet<GotchaPlayer>();
		for(GotchaPlayer p : friends)
			if(!checked.contains(p)) {
				if(p.target() != null && p.target().isFriendly()) {
					Chain c = new Chain(p);
					chains.add(c);
					checked.addAll(c.players);
				}
			}
		return chains;
	}
}
