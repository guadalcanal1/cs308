package com.htmlhigh5;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.htmlhigh5.model.User;

public class LoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			ServletHelper.loadPageStart(request, response);
			request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").include(request, response);
			ServletHelper.loadPageEnd(request, response);

		} catch (ServletException e) {
			e.printStackTrace();
		}
	}
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		User u = AuthController.login(request, response);
		if(u != null){
			response.sendRedirect("/home");
		}
	}
}