package com.htmlhigh5;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.htmlhigh5.model.Group;

public class GroupController {
	public static Group create(HttpServletRequest request, HttpServletResponse response) {
		String name = request.getParameter("name").toString();
		String password = request.getParameter("password").toString();
		String password2 = request.getParameter("password2").toString();
		String salt = BCrypt.gensalt();
		String hashed = BCrypt.hashpw(password, salt);
		String[] cols = {"name", "password"};
		String[] values = {name, hashed};
		if(AuthController.isAuthenticated(request))
			if(password.equals(password2)){
				if(Group.where("name", "=", name).size() == 0){
					Group.create(cols, values);
					Group g = Group.where("name", "=", name).get(0);
					g.addAdmin(AuthController.user(request));
					return g;
				}
			}
		return null;
	}
}
