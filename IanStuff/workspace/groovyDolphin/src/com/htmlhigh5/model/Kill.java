package com.htmlhigh5.model;

import java.util.ArrayList;

public class Kill extends Model{
	
	private static String tableName = "kills";
	public static String[] columnNames = {"id", "killer_id", "killed_id"};
	
	public Kill(ArrayList<Object> vals){
		super(vals, columnNames);
	}
	
	public static ArrayList<Kill> where(String column, String modifier, Object value){
		return whereInTable(column, modifier, value, tableName, columnNames, Kill.class);
	}
	
	public static ArrayList<Kill> all(){
		return allInTable(tableName, columnNames, Kill.class);
	}

	public static Kill find(int id){
		return findInTable(id, tableName, columnNames, Kill.class);
	}
	
	public GotchaPlayer killer(){
		return GotchaPlayer.find(this.integer("killer_id"));
	}
	
	public GotchaPlayer killed(){
		return GotchaPlayer.find(this.integer("killed_id"));
	}
	
	@Override
	public String toString(){
		return "[Kill||| killer_id: " + this.integer("killer_id") + " killed_id: " + this.integer("killed_id") + "]";
	}
	

	public static String getModelName() {
		return "kill";
	}
}
