package com.htmlhigh5.model;

import java.util.ArrayList;

public class Group extends Model{
	
	private static String tableName = "groups";
	public static String[] columnNames = {"id", "name", "password"};
	
	public Group(ArrayList<Object> vals){
		super(vals, columnNames);
	}
	
	public static ArrayList<Group> where(String column, String modifier, Object value){
		return whereInTable(column, modifier, value, tableName, columnNames, Group.class);
	}
	
	public static ArrayList<Group> all(){
		return allInTable(tableName, columnNames, Group.class);
	}
	
	public static Group find(int id){
		return findInTable(id, tableName, columnNames, Group.class);
	}
	
	public void addAdmin(User u) {
		System.out.println(this.str("id"));
		System.out.println(this.integer("id"));
		GroupAdmin.create(new String[]{"group_id",  "user_id"}, new Object[]{this.integer("id"), u.integer("id")});
		addUser(u);
	}

	public void addUser(User u){
		GroupUser.create(new String[]{"group_id",  "user_id"}, new Object[]{this.integer("id"), u.integer("id")});
	}
	
	public ArrayList<GotchaPlayer> players(){
		return GotchaPlayer.where("group_id", "=", this.integer("id"));
	}
	
	public static Group create(String[] columnNames, Object[] values) {
		return create(columnNames, values, tableName, Group.class);
	}
	
	@Override
	public String toString(){
		return "[Group||| name: " + this.str("name") + "]";
	}
	

	public static String getModelName() {
		return "group";
	}
}
