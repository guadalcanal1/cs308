package com.htmlhigh5.model;

import java.util.ArrayList;

public class User extends Model{
	
	private static String tableName = "users";
	public static String[] columnNames = {"id", "username", "password", "email_address", "group_id", "player_id", "auth_token", "salt"};
	
	public User(ArrayList<Object> vals){
		super(vals, columnNames);
	}
	
	public static ArrayList<User> where(String column, String modifier, Object value){
		return whereInTable(column, modifier, value, tableName, columnNames, User.class);
	}
	
	public static ArrayList<User> where(String[] columns, String[] modifiers, Object[] values) {
		return Model.whereInTable(columns, modifiers, values, tableName, columnNames, User.class);
	}
	
	public static ArrayList<User> all(){
		return allInTable(tableName, columnNames, User.class);
	}

	public static User find(int id){
		return findInTable(id, tableName, columnNames, User.class);
	}
	
	public User update(String[] columnNames, Object[] values) {
		return update(columnNames, values, tableName, User.class, this.integer("id"));
	}
	
	public GotchaPlayer player(){
		return GotchaPlayer.find(this.integer("player_id"));
	}
	
	public static User create(String[] columnNames, Object[] values) {
		return create(columnNames, values, tableName, User.class);
	}
	
	public ArrayList<Group> memberGroups(){
		ArrayList<GroupUser> groupUsers = GroupUser.where("user_id", "=", this.integer("id"));
		ArrayList<Group> groups = new ArrayList<Group>();
		for(GroupUser gu : groupUsers){
			Group g = Group.find(gu.integer("group_id"));
			if(g != null)
				groups.add(g);
		}
		return groups;
	}
	
	public ArrayList<Group> adminGroups(){
		ArrayList<GroupAdmin> groupUsers = GroupAdmin.where("user_id", "=", this.integer("id"));
		ArrayList<Group> groups = new ArrayList<Group>();
		for(GroupAdmin gu : groupUsers){
			Group g = Group.find(gu.integer("group_id"));
			if(g != null)
				groups.add(g);
		}
		return groups;
	}
	
	@Override
	public String toString(){
		return "[User||| username: " + this.str("username") + "]";
	}
	
	public static String getModelName() {
		return "user";
	}
}
