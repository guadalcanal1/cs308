package com.htmlhigh5.model;

import java.util.ArrayList;

public abstract class APIModel {
    static String modelName = "";

    public APIModel() {
    	modelName = getModelName();
    }

    public abstract String getModelName();
}
