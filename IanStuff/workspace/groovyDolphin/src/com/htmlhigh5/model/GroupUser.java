package com.htmlhigh5.model;

import java.util.ArrayList;

public class GroupUser extends Model{
	
	private static String tableName = "group_users";
	public static String[] columnNames = {"id", "user_id", "group_id"};
	
	public GroupUser(ArrayList<Object> vals){
		super(vals, columnNames);
	}
	
	public static ArrayList<GroupUser> where(String column, String modifier, Object value){
		return whereInTable(column, modifier, value, tableName, columnNames, GroupUser.class);
	}
	
	public static ArrayList<GroupUser> all(){
		return allInTable(tableName, columnNames, GroupUser.class);
	}

	public static GroupUser find(int id){
		return findInTable(id, tableName, columnNames, GroupUser.class);
	}
	
	public static GroupUser create(String[] columnNames, Object[] values){
		return create(columnNames, values, tableName, GroupUser.class);
	}
	
	public User user(){
		return User.find(this.integer("user_id"));
	}
	
	public Group group(){
		return Group.find(this.integer("group_id"));
	}
	
	@Override
	public String toString(){
		return "[Group User||| user ID: " + this.integer("user_id") + " Group ID: " + this.integer("group_id") + "]";
	}
	

	public static String getModelName() {
		return "group-user";
	}
}
