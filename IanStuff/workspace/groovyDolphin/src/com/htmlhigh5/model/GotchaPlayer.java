package com.htmlhigh5.model;

import java.util.ArrayList;

public class GotchaPlayer extends Model implements Comparable<GotchaPlayer> {

	protected static String tableName = "gotcha_players";
	public static String[] columnNames = { "id", "first_name", "middle_name", "last_name", "is_friendly", "is_alive",
			"target_id", "targeted_by_id" };

	public GotchaPlayer(ArrayList<Object> vals) {
		super(vals, columnNames);
	}

	public static ArrayList<GotchaPlayer> where(String column, String modifier, Object value) {
		return Model.whereInTable(column, modifier, value, tableName, columnNames, GotchaPlayer.class);
	}

	public static ArrayList<GotchaPlayer> where(String[] columns, String[] modifiers, Object[] values) {
		return Model.whereInTable(columns, modifiers, values, tableName, columnNames, GotchaPlayer.class);
	}

	public static ArrayList<GotchaPlayer> all() {
		return Model.allInTable(tableName, columnNames, GotchaPlayer.class);
	}

	public Group group() {
		ArrayList<Group> list = Group.where("id", "=", this.integer("group_id"));
		return list.size() > 0 ? list.get(0) : null;
	}

	public static GotchaPlayer find(int id) {
		return findInTable(id, tableName, columnNames, GotchaPlayer.class);
	}

	public GotchaPlayer target() {
		return GotchaPlayer.find(this.integer("target_id"));
	}

	public GotchaPlayer targetedBy() {
		return GotchaPlayer.find(this.integer("targeted_by_id"));
	}

	public String fullName() {
		String f = this.str("first_name") + " ";
		String m = this.str("middle_name") != null ? this.str("middle_name")  + " " : "";
		String l = this.str("last_name");
		return f + m + l;
	}

	public User user() {
		ArrayList<User> list = User.where("player_id", "=", this.integer("id"));
		if (list.size() == 0)
			return null;
		return list.get(0);
	}

	public boolean isAlive() {
		return this.bool("is_alive");
	}

	public boolean isFriendly() {
		return this.bool("is_friendly");
	}

	public int killCount() {
		return Kill.where("killer_id", "=", this.integer("id")).size();
	}

	public int nameCount() {
		ArrayList<Name> names = Name.where("player_id", "=", this.integer("id"));
		if (names.size() == 0)
			return 0;
		return names.get(0).integer("name_count");
	}

	@Override
	public String toString() {
		return "[Gotcha Player||| Name: " + this.fullName() + "]";
	}

	@Override
	public int compareTo(GotchaPlayer gp) {
		// System.out.println("Comparing");
		// System.out.println(gp.fullName() + " to " + this.fullName() + " = " +
		// gp.fullName().compareTo(this.fullName()));
		return (gp.fullName().compareTo(this.fullName()));
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof GotchaPlayer && ((GotchaPlayer) o).integer("id") == this.integer("id"))
			return true;
		return false;
	}
	
	public boolean equals(GotchaPlayer g){
		return g.integer("id") == this.integer("id");
	}
	
	@Override
	public int hashCode(){
		return this.integer("id");
	}
	

	public static String getModelName() {
		return "gotcha-player";
	}
}
