package com.htmlhigh5.model;

import java.util.ArrayList;

public class Name extends Model{
	
	private static String tableName = "player_names";
	public static String[] columnNames = {"id", "player_id", "name_count"};
	
	public Name(ArrayList<Object> vals){
		super(vals, columnNames);
	}
	
	public static ArrayList<Name> where(String column, String modifier, Object value){
		return whereInTable(column, modifier, value, tableName, columnNames, Name.class);
	}
	
	public static ArrayList<Name> all(){
		return allInTable(tableName, columnNames, Name.class);
	}

	public static Name find(int id){
		return findInTable(id, tableName, columnNames, Name.class);
	}
	
	public GotchaPlayer player(){
		return GotchaPlayer.find(this.integer("player_id"));
	}
	
	@Override
	public String toString(){
		return "[Name||| player_id: " + this.integer("player_id") + " Count: " + this.integer("name_count") + "]";
	}
	

	public static String getModelName() {
		return "name";
	}
}
