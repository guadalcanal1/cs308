package com.htmlhigh5.model;

import java.util.ArrayList;

public class GroupAdmin extends Model{
	
	private static String tableName = "group_admins";
	public static String[] columnNames = {"id", "user_id", "group_id"};
	
	public GroupAdmin(ArrayList<Object> vals){
		super(vals, columnNames);
	}
	
	public static ArrayList<GroupAdmin> where(String column, String modifier, Object value){
		return whereInTable(column, modifier, value, tableName, columnNames, GroupAdmin.class);
	}
	
	public static ArrayList<GroupAdmin> all(){
		return allInTable(tableName, columnNames, GroupAdmin.class);
	}

	public static GroupAdmin find(int id){
		return findInTable(id, tableName, columnNames, GroupAdmin.class);
	}
	
	public static GroupAdmin create(String[] columnNames, Object[] values){
		return create(columnNames, values, tableName, GroupAdmin.class);
	}
	
	public User user(){
		return User.find(this.integer("user_id"));
	}
	
	public Group group(){
		return Group.find(this.integer("group_id"));
	}
	
	@Override
	public String toString(){
		return "[Group Admin||| user ID: " + this.integer("user_id") + " Group ID: " + this.integer("group_id") + "]";
	}
	

	public static String getModelName() {
		return "group-admin";
	}
}
