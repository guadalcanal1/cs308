package com.htmlhigh5;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class IndexServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		int groupID = 1;
		request.setAttribute("friends", GotchaPlayerController.getFriends(groupID));
		request.setAttribute("targets", GotchaPlayerController.getTargeters(groupID));
		request.setAttribute("targeters", GotchaPlayerController.getTargets(groupID));
		request.setAttribute("chains", GotchaPlayerController.getChains(groupID));
		try {
			ServletHelper.loadPageStart(request, response);
			request.getRequestDispatcher("/WEB-INF/jsp/index.jsp").include(request, response);
			ServletHelper.loadPageEnd(request, response);

		} catch (ServletException e) {
			e.printStackTrace();
		}
	}
}