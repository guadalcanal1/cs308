package com.htmlhigh5;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.htmlhigh5.model.*;

public class APIServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	ArrayList<String> apiModels = new ArrayList<String>();
	

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String r = request.getRequestURI();
		String[] colNames = {};
		ArrayList<? extends Model> all = new ArrayList<Model>();
		if(r.contains(GotchaPlayer.getModelName())) {
			colNames = GotchaPlayer.columnNames;
			all = GotchaPlayer.all();
		} else if(r.contains(Kill.getModelName())) {
			colNames = Kill.columnNames;
			all = Kill.all();
		}
		System.out.println("Path: " + request.getRequestURI());
		System.out.println("Query: " + request.getQueryString());
		String ret = "[";
		if(all.size() == 0) {
			response.getWriter().print("[]");
			return;
		}
		for(Model m : all) {
			ret += "{";
			for(String s : colNames) {
				ret += "\"" + s + "\": \"" + m.obj(s) + "\",";
			}
			ret = ret.substring(0, ret.length() - 1);
			ret += "},";
		}
		ret = ret.substring(0, ret.length() - 1);
		ret += "]";
		response.getWriter().print(ret);
	}
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
	}
}