package com.htmlhigh5;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.htmlhigh5.model.User;

public class ServletHelper {
	protected static void loadPageStart(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		User u = AuthController.user(request);
		request.setAttribute("user", u);
		Cookie[] cookies = request.getCookies();
		String language = "en";
		String country = "US";
		if(cookies != null)
			for(Cookie c : cookies){
				if(c.getName().equals("language"))
					language = c.getValue();
				if(c.getName().equals("country"))
					country = c.getValue();
			}
		System.out.println("Resolved language and country: " + language + ", " + country);
		Locale locale = new Locale(language, country);
		ResourceBundle messages = ResourceBundle.getBundle("MessagesBundle", locale);
		Set<String> keys = messages.keySet();
		HashMap<String, String> allMessages = new HashMap<String, String>();
		for(String s : keys) {
			System.out.println(s + ": " + messages.getString(s));
			allMessages.put(s, messages.getString(s));
		}
		request.setAttribute("messages", allMessages);
		request.setCharacterEncoding("UTF-8");
		request.getRequestDispatcher("/WEB-INF/jsp/headStart.jsp").include(request, response);
		request.getRequestDispatcher("/WEB-INF/jsp/headEnd.jsp").include(request, response);
		request.getRequestDispatcher("/WEB-INF/jsp/bodyStart.jsp").include(request, response);
	}

	protected static void loadPageEnd(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/jsp/bodyEnd.jsp").include(request, response);
	}
}
