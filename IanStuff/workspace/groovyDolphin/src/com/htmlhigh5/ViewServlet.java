package com.htmlhigh5;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ViewServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			ServletHelper.loadPageStart(request, response);
			request.getRequestDispatcher("/WEB-INF/jsp/me.jsp").include(request, response);
			ServletHelper.loadPageEnd(request, response);

		} catch (ServletException e) {
			e.printStackTrace();
		}
	}
}