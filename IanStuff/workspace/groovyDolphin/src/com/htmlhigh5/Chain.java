package com.htmlhigh5;
import java.util.ArrayList;

import com.htmlhigh5.model.GotchaPlayer;

public class Chain {
	public ArrayList<GotchaPlayer> players = new ArrayList<GotchaPlayer>();
	public GotchaPlayer topPlayer, bottomPlayer;
	int length = 0;
	int names = 0;
	
	public Chain(GotchaPlayer p) {
		this.topPlayer = p;
		GotchaPlayer gp = p;
		while(gp != null) {
			this.players.add(gp);
			this.length++;
			this.names += gp.nameCount();
			if(gp.target() == null || !gp.target().isFriendly())
				break;
			gp = gp.target();
		}
		this.bottomPlayer = gp;
	}
	
	public int getLength() {
		return this.length;
	}
	
	public int getNames() {
		return this.names;
	}
	
	public ArrayList<GotchaPlayer> getPlayers(){
		return this.players;
	}
	
	public boolean isLast(GotchaPlayer p) {
		return p.equals(this.bottomPlayer);
	}
	
	public boolean isFirst(GotchaPlayer p) {
		return p.equals(this.topPlayer);
	}
}
