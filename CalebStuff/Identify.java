import java.io.Serializable;

/**
 *
 * @author Caleb
 */
class Identify implements Serializable {
    int direction;
    int id;
    Identify() {
        
    }
    Identify(int direction, int id) {
        this.direction = direction;
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public int getDirection() {
        return direction;
    }
}