/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.*;
import java.io.*;
import java.util.Date;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javafx.application.Application;
import javafx.stage.Stage;
/**
 *
 * @author Caleb
 */
public class MicrocosmFriend extends Application{
    static ServerSocket special = null;
    static int counter = -1;
  
    static boolean twoReached = false;
    static Situation ensitu;
    
    static Identify helper;
    static Lock lock = new ReentrantLock();
    
    
    public void start(Stage primaryStage) {
        primaryStage.show();
        
        ensitu = new Situation();
        
        new Thread(() -> {
            
            try {
                special = new ServerSocket(7000);
            System.out.println("Server started at "+ new Date());
               while (true) {
                   Socket socket = special.accept();
                   counter++;
                   new Thread(new Player(socket,counter)).start();
               }
            }
            
            catch (Exception ex) {}
            
        }).start();
          
    }
    public static void main(String[] args) {
        Application.launch(args);
    }
    
    public static void messWithSituation(int decision, Identify helper, ObjectOutputStream tClient, int counter) {
        lock.lock();
        System.out.println("got inside");
            /*
            0 is send
            1 is writeTo
            2 is addPlayer
            */
            try {
                if (decision == 0) {
                  tClient.reset();
                  tClient.writeUnshared(ensitu);
                    System.out.println("A");
                }
                else if (decision == 1) {
                    ensitu.updatePlayer(helper);
                    System.out.println("B");
                }
                
                
            }
            catch (Exception ex) {
                System.out.println("Something changed");
            }
            
            finally {
                lock.unlock();
            }
            
        
    }
    
    // about the same as book's HandleClient
    
    private class Player implements Runnable {
        private Socket socket;
        int id;
        Player() {
            
        }
        
        Player(Socket socket, int counter) {
            this.socket = socket;
            id = counter;
        }
        
        public void run() {
            try {
                /// System.out.println("Server accepted at " + new Date());
        
               
                
                ObjectOutputStream toClient = new ObjectOutputStream(socket.getOutputStream());
                toClient.flush();
                
                ObjectInputStream fromClient = new ObjectInputStream(socket.getInputStream());
                
                DataOutputStream outputToClient = new DataOutputStream(socket.getOutputStream());
                
                outputToClient.writeInt(id);
                
                while (true) {
                    messWithSituation(1,(Identify)fromClient.readObject(), toClient, id);
                    messWithSituation(0, new Identify(2,0),toClient,id);
                }
                
            }
            catch (Exception ex) {
                
            }
            
            
        }
    }
    
    
}



