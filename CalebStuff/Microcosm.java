/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.ArrayList;
import java.util.HashSet;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.stage.Stage;
import java.io.*;
import java.net.*;
import java.util.List;

/**
 *
 * @author Caleb
 */
public class Microcosm extends Application {
    
    DataInputStream fromServer;
    DataOutputStream toServer;
    ObjectOutputStream update;
    ObjectInputStream resend;
    Situation ensitu;
    boolean gameOver = false;
    static Circle curl;
    static Veh v = new Veh();
    static boolean[] direction = new boolean[4];
    static Button[] navigation = new Button[4];
    static int timeInterval = 150;
    static int increaseInterval = 10;
    static HBox controls = new HBox();
    static BorderPane Bp = new BorderPane();
    static Pane p = new Pane();
    ////////////////////GUI///////////////////
    public void start(Stage primaryStage) {
        initialize(primaryStage);
        primaryStage.show();
        
    }
    
    //////////////////Initialization////////
    public void initialize(Stage primaryStage){
        
        try {
        Socket sock = new Socket("localhost", 7000);
        System.out.println("accepted");
        update = new ObjectOutputStream(sock.getOutputStream());
        resend = new ObjectInputStream(sock.getInputStream());
        toServer = new DataOutputStream(sock.getOutputStream());
        fromServer = new DataInputStream(sock.getInputStream());
        
        }

        catch (Exception ex) {
            ex.printStackTrace();
        }
        System.out.println("Something is happening");
        Bp.setCenter(p);
        Bp.setBottom(controls);        
        curl = new Circle(0, 100, 10);

        navigation[0] = new Button("UP");
        navigation[1] = new Button("Down");
        navigation[2] = new Button("right");
        navigation[3] = new Button("left");
        
        for (int i = 0; i < 4; i++) {
            controls.getChildren().add(navigation[i]);
        }
        
        
        primaryStage.setScene(new Scene(Bp, 500,500));
        changeDirection(2); ///// car will start going right
       
        setRules();
        primaryStage.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    
                    int id = fromServer.readInt();
                    System.out.println(id);
                    while (!gameOver) {
                        Platform.runLater(new Runnable() {
                            public void run() {
                                try {
                                    
                                    update.writeObject(new Identify(sendDirection(),id));
                                    update.flush();
                                    ensitu = (Situation)resend.readUnshared();
                                    redisplay();
                                    
                                }
                                catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                                
                            }
                        });
                        Thread.sleep(timeInterval);
                    }
                }
                catch (Exception ex) {
                    
                }
            }
        }).start();
        
       
    }
    
    
    public void redisplay() {
        int[] theList;
        Coordinate[] zaList;
        p.getChildren().clear();
        for (int i = 0; i < 2; i++) {
            theList = ensitu.rArray(i);
            zaList = ensitu.rPath(i);
            p.getChildren().add(new Circle(theList[0], theList[1], theList[2]));
            putLinesTogether(zaList);
            
        }
    }
    
    public int sendDirection() {
       for (int i = 0; i < 4; i++) {
            if (direction[i] == true)
                return i;
       }
                return 2;
    }
    
    /////////////beginning///////////////

    //////////////////ruleset//////////////
    public void setRules() {
        navigation[0].setOnAction(new upHandler());
        navigation[1].setOnAction(new downHandler());
        navigation[2].setOnAction(new rightHandler());
        navigation[3].setOnAction(new leftHandler());
    }
    public void changeDirection(int number) {
        for (int i = 0; i < 4; i++) 
            direction[i] = false;
        direction[number] = true;  
    }
    /*
    public void assembleLine(List path) {
        if (path.size() > 1) {
        Point2D[] points = new Point2D[path.size()];
        Line[] lines = new Line[path.size() - 1];
            for (int i = 0; i < path.size(); i++) {
                points[i] = new Point2D(path.get(i).x,path.get(i).y));
            }
            for (int i = 0; i < path.size()-1; i++) {
                lines[i] = new Line(points[i].getX(),points[i].getY(), points[i+1].getX(), points[i+1].getY());
                p.getChildren().add(lines[i]);
            }
            
            HashSet<Point2D> set = new HashSet<>();
            for (int i = 0; i < path.size(); i++) {
                set.add(points[i]);
            }
            if (set.size()!= path.size()) {
                gameOver = true;
                System.out.println("Game Over");
            }
        }   
    }
*/
    
    public void putLinesTogether(Coordinate[] path) {
        Line[] lines = new Line[path.length - 1];
        for (int i = 0; i < path.length - 1; i++) {
            lines[i] = new Line(path[i].x, path[i].y, path[i+1].x, path[i+1].y);
            p.getChildren().add(lines[i]);
        }
    }
    
    
    ///////////////Handlers
    class upHandler implements EventHandler<ActionEvent> {
         @Override
         public void handle(ActionEvent e) {
             if(!direction[1])
                 changeDirection(0);
         }
     }
     class downHandler implements EventHandler<ActionEvent> {
         @Override
         public void handle(ActionEvent e) {
             if(!direction[0])
             changeDirection(1);        
         }
     }
     class leftHandler implements EventHandler<ActionEvent> {
         @Override
         public void handle(ActionEvent e) {
             if (!direction[2])    
             changeDirection(3);
         }
     }
     class rightHandler implements EventHandler<ActionEvent> {
         @Override
         public void handle(ActionEvent e) {
             if(!direction[3])
                 changeDirection(2);
         }
     }
     
     public static void main(String[] args) {
        Application.launch(args);
    }
}


