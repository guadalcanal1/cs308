/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Caleb
 */
class Situation implements Serializable {
    Veh[] play = new Veh[1];
    
    ArrayList<Veh> chump = new ArrayList();
    List easy = Collections.synchronizedList(chump);
    Situation() {
        easy.add(new Veh(100, 100, 10));
        easy.add(new Veh(200, 200, 10));
    }
    
    public int[] rArray(int id) {
        
        Veh v = (Veh)easy.get(id);
        return v.getQualities();
        
    }
    
    public Coordinate[] rPath(int id) {
        Veh v = (Veh)easy.get(id);
        return v.getPath();
    }
    
    
    
    public void updatePlayer( Identify helper) {
        chump.get(helper.getId()).setDirection(helper.getDirection());
        chump.get(helper.getId()).move();
        easy = Collections.synchronizedList(chump);
        System.out.println("player moved");
       
        
    }
    
   
}